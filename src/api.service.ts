/* eslint-disable no-console */
import axios from 'axios';

import { Device } from './device';

class ApiService {
  private readonly url: string;

  constructor(url: string) {
    this.url = url;

    this.getUrl = this.getUrl.bind(this);

    this.getDevice = this.getDevice.bind(this);
    this.fill = this.fill.bind(this);
  }

  public getUrl() {
    return this.url;
  }

  public async getDevice(): Promise<Device | null>{
    const infos = `ApiService.getDevice(${this.url})`;
    console.info(`${new Date()} :: ${infos}`);

    let result: Device | null = null;
    try {
      const { data, status} = await axios.get(this.url);

      if (status !== 200) {
        throw new Error('NetworkError');
      }
      console.log({ data });
      result = data.result as Device;

      console.info(`${new Date()} :: ${infos} ::`, { result });
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  public async fill(color: string): Promise<boolean>{
    const infos = `ApiService.fill(${this.url}, ${color})`;
    console.info(`${new Date()} :: ${infos}`);

    let result = false;

    try {
      const { data, status } = await axios.get(`${this.url}/fill/${color}`);

      if (status !== 200) {
        throw new Error('NetworkError');
      }
      if (data.errors.length !== 0) {
        throw new Error(data.errors);
      }

      result = true;
      console.info(`${new Date()} :: ${infos} ::`, result);
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  public async arrayFill(colors: string[]): Promise<boolean>{
    const infos = `ApiService.arrayFill(${this.url}, ${colors})`;
    console.info(`${new Date()} :: ${infos}`);

    let result = false;

    try {

      const { data, status } = await axios.post(`${this.url}/array_fill`, { colors });

      if (status !== 200) {
        throw new Error('NetworkError');
      }
      if (data.errors.length !== 0) {
        throw new Error(data.errors);
      }

      result = true;
      console.info(`${new Date()} :: ${infos} ::`, result);
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  private handleError(infos: string, error: unknown): void {

    if ((error as Error).name === 'NetworkError') {
      console.error(`${new Date()} :: ${infos} :: ERROR : `, 'NetworkError');
    } else{
      console.error(`${new Date()} :: ${infos} :: ERROR : `, (error as Error).message);
    }
    console.log((error as Error).stack);
  }
}

export { ApiService };