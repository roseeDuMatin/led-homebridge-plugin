type Device = {
    guid: string;
    name: string;
    colors: string;
};

export { Device };