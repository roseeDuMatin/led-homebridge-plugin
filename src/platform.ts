import {
  API,
  DynamicPlatformPlugin,
  Logger,
  PlatformName,
  PlatformAccessory,
  PlatformConfig,
  Service,
  Characteristic,
  PlatformIdentifier,
  BridgeConfiguration,
} from 'homebridge';

import { PLATFORM_NAME, PLUGIN_NAME } from './settings';
import { LEDAccessory } from './platformAccessory';
import { ApiService } from './api.service';
import { Device } from './device';


interface CustomConfig extends PlatformConfig {
  platform: PlatformName | PlatformIdentifier;
  name?: string;
  url?: string;
  _bridge?: BridgeConfiguration;
}


export class ExampleHomebridgePlatform implements DynamicPlatformPlugin {
  public readonly Service: typeof Service = this.api.hap.Service;
  public readonly Characteristic: typeof Characteristic = this.api.hap.Characteristic;
  public apiService!: ApiService;

  public readonly accessories: PlatformAccessory[] = [];

  constructor(
    public readonly log: Logger,
    public readonly config: CustomConfig,
    public readonly api: API,
  ) {
    this.log.debug('Finished initializing platform:', this.config.name);

    if(!this.config.url) {
      return;
    }

    this.apiService = new ApiService(this.config.url);

    this.api.on('didFinishLaunching', () => {
      log.debug('Executed didFinishLaunching callback');
      this.discoverDevices();
    });
  }

  configureAccessory(accessory: PlatformAccessory) {
    this.log.info('Loading accessory from cache:', accessory.displayName);
    this.accessories.push(accessory);
  }

  async discoverDevices() {
    const result = await this.apiService.getDevice();
    if (!result) {
      return;
    }

    const device = result as Device;
    const uuid = this.api.hap.uuid.generate(device.guid);
    const existingAccessory = this.accessories.find(accessory => accessory.UUID === uuid);

    if (existingAccessory) {
      this.log.info('Restoring existing accessory from cache:', existingAccessory?.displayName);
      new LEDAccessory(this, existingAccessory, this.apiService);
    }else {
      this.log.info('Adding new accessory:', device.name);

      const accessory = new this.api.platformAccessory(device.name, uuid);
      accessory.context.device = device;

      new LEDAccessory(this, accessory, this.apiService);
      this.api.registerPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, [accessory]);
    }
    // // loop over the discovered devices and register each one if it has not already been registered
    // for (const device of exampleDevices) {

    //   // generate a unique id for the accessory this should be generated from
    //   // something globally unique, but constant, for example, the device serial
    //   // number or MAC address
    //   const uuid = this.api.hap.uuid.generate(device.exampleUniqueId);

    //   // see if an accessory with the same uuid has already been registered and restored from
    //   // the cached devices we stored in the `configureAccessory` method above
    //   const existingAccessory = this.accessories.find(accessory => accessory.UUID === uuid);

    //   if (existingAccessory) {
    //     // the accessory already exists
    //     this.log.info('Restoring existing accessory from cache:', existingAccessory.displayName);

    //     // if you need to update the accessory.context then you should run `api.updatePlatformAccessories`. eg.:
    //     // existingAccessory.context.device = device;
    //     // this.api.updatePlatformAccessories([existingAccessory]);

    //     // create the accessory handler for the restored accessory
    //     // this is imported from `platformAccessory.ts`
    //     new ExamplePlatformAccessory(this, existingAccessory);

    //     // it is possible to remove platform accessories at any time using `api.unregisterPlatformAccessories`, eg.:
    //     // remove platform accessories when no longer present
    //     // this.api.unregisterPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, [existingAccessory]);
    //     // this.log.info('Removing existing accessory from cache:', existingAccessory.displayName);
    //   } else {
    //     // the accessory does not yet exist, so we need to create it
    //     this.log.info('Adding new accessory:', device.exampleDisplayName);

    //     // create a new accessory
    //     const accessory = new this.api.platformAccessory(device.exampleDisplayName, uuid);

    //     // store a copy of the device object in the `accessory.context`
    //     // the `context` property can be used to store any data about the accessory you may need
    //     accessory.context.device = device;

    //     // create the accessory handler for the newly create accessory
    //     // this is imported from `platformAccessory.ts`
    //     new ExamplePlatformAccessory(this, accessory);

    //     // link the accessory to your platform
    //     this.api.registerPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, [accessory]);
    //   }
    // }
  }
}
