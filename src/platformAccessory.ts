import { Service, PlatformAccessory, CharacteristicValue } from 'homebridge';
// import { Service, PlatformAccessory, CharacteristicValue, Characteristic, Formats, Perms } from 'homebridge';

import { ApiService } from './api.service';

import { ExampleHomebridgePlatform } from './platform';


// export class ExampleCharacteristic extends Characteristic {

//   static readonly UUID: string = '000000CE-0000-1000-8000-0026ABCDEF01';

//   constructor() {
//     super('Shield Level', ShieldLevel.UUID);
//     this.setProps({
//       format: Formats.UINT32,
//       maxValue: 100,
//       minValue: 0,
//       minStep: 1,
//       perms: [Perms.READ, Perms.WRITE, Perms.NOTIFY],
//     });
//     this.value = this.getDefaultValue();
//   }
// }

export class LEDAccessory {
  private lb_service: Service;
  // private c_service: Service;
  private state = {
    On: false,
    Colors: '[]',
  };


  constructor(
    private readonly platform: ExampleHomebridgePlatform,
    private readonly accessory: PlatformAccessory,
    private readonly apiService: ApiService,
  ) {

    this.accessory.getService(this.platform.Service.AccessoryInformation)!
      .setCharacteristic(this.platform.Characteristic.Manufacturer, 'MOC_ESGI')
      .setCharacteristic(this.platform.Characteristic.Model, 'Raspberry-LED')
      .setCharacteristic(this.platform.Characteristic.SerialNumber, 'Default-Serial');

    // const serviceUUID = '00000200-0000-1000-8000-0026BB765291';
    // const serviceName = 'LED Controller';
    // const service = new LedService(serviceUUID, serviceName);

    // this.accessory.getService(service.UUID) || this.accessory.addService(service);

    // const service = new Service(serviceUUID, serviceName);

    this.lb_service = this.accessory.getService(this.platform.Service.Lightbulb) ||
                      this.accessory.addService(this.platform.Service.Lightbulb);
    // this.c_service = this.accessory.getService(this.platform.Service.AccessoryMetrics) ||
    //                   this.accessory.addService(this.platform.Service.AccessoryMetrics);
    // set the service name, this is what is displayed as the default name on the Home app
    // in this example we are using the name we stored in the `accessory.context` in the `discoverDevices` method.
    this.lb_service.setCharacteristic(this.platform.Characteristic.Name, accessory.context.device.exampleDisplayName);



    // each service must implement at-minimum the "required characteristics" for the given service type
    // see https://developers.homebridge.io/#/service/Lightbulb

    this.lb_service.getCharacteristic(this.platform.Characteristic.On)
      .onSet(this.setOn.bind(this))
      .onGet(this.getOn.bind(this));

    this.lb_service.getCharacteristic(this.platform.Characteristic.Dura)
      // .onSet(this.setColor.bind(this))
      .onGet(this.getColor.bind(this));
    // this.service.getCharacteristic(this.platform.Characteristic.Brightness)
    //   .onSet(this.setBrightness.bind(this));

    /**
     * Creating multiple services of the same type.
     *
     * To avoid "Cannot add a Service with the same UUID another Service without also defining a unique 'subtype' property." error,
     * when creating multiple services of the same type, you need to use the following syntax to specify a name and subtype id:
     * this.accessory.getService('NAME') || this.accessory.addService(this.platform.Service.Lightbulb, 'NAME', 'USER_DEFINED_SUBTYPE_ID');
     *
     * The USER_DEFINED_SUBTYPE must be unique to the platform accessory (if you platform exposes multiple accessories, each accessory
     * can use the same sub type id.)
     */


    // Example: add two "motion sensor" services to the accessory
    // const motionSensorOneService = this.accessory.getService('Motion Sensor One Name') ||
    //   this.accessory.addService(this.platform.Service.MotionSensor, 'Motion Sensor One Name', 'YourUniqueIdentifier-1');

    // const motionSensorTwoService = this.accessory.getService('Motion Sensor Two Name') ||
    //   this.accessory.addService(this.platform.Service.MotionSensor, 'Motion Sensor Two Name', 'YourUniqueIdentifier-2');

    /**
     * Updating characteristics values asynchronously.
     *
     * Example showing how to update the state of a Characteristic asynchronously instead
     * of using the `on('get')` handlers.
     * Here we change update the motion sensor trigger states on and off every 10 seconds
     * the `updateCharacteristic` method.
     *
     */
    // let motionDetected = false;
    setInterval(async () => {

      this.lb_service.updateCharacteristic(this.platform.Characteristic.Hue, await this.getColor());
      // // EXAMPLE - inverse the trigger
      // motionDetected = !motionDetected;

      // // push the new value to HomeKit
      // motionSensorOneService.updateCharacteristic(this.platform.Characteristic.MotionDetected, motionDetected);
      // motionSensorTwoService.updateCharacteristic(this.platform.Characteristic.MotionDetected, !motionDetected);

      // this.platform.log.debug('Triggering motionSensorOneService:', motionDetected);
      // this.platform.log.debug('Triggering motionSensorTwoService:', !motionDetected);
    }, 10000);
  }

  /**
   * Handle "SET" requests from HomeKit
   * These are sent when the user changes the state of an accessory, for example, turning on a Light bulb.
   */
  async setOn(value: CharacteristicValue) {
    const isOn = !this.state.On;

    const result = await this.apiService.fill(isOn ? 'WHITE' : 'BLANK');
    if (result === null) {
      throw new this.platform.api.hap.HapStatusError(this.platform.api.hap.HAPStatus.SERVICE_COMMUNICATION_FAILURE);
    }
    this.state.On = value as boolean;

    this.platform.log.debug('Set Characteristic On ->', value);
  }

  /**
   * Handle the "GET" requests from HomeKit
   * These are sent when HomeKit wants to know the current state of the accessory, for example, checking if a Light bulb is on.
   *
   * GET requests should return as fast as possbile. A long delay here will result in
   * HomeKit being unresponsive and a bad user experience in general.
   *
   * If your device takes time to respond you should update the status of your device
   * asynchronously instead using the `updateCharacteristic` method instead.

   * @example
   * this.service.updateCharacteristic(this.platform.Characteristic.On, true)
   */
  async getOn(): Promise<CharacteristicValue> {
    const result = await this.apiService.getDevice();
    if (result === null) {
      throw new this.platform.api.hap.HapStatusError(this.platform.api.hap.HAPStatus.SERVICE_COMMUNICATION_FAILURE);
    }

    const isOn = result !== null && result.colors !== 'BLANK';

    this.platform.log.debug('Get Characteristic On ->', isOn);
    return isOn;
  }

  /**
   * Handle "SET" requests from HomeKit
   * These are sent when the user changes the state of an accessory, for example, changing the Brightness
   */
  // async setBrightness(value: CharacteristicValue) {
  //   // implement your own code to set the brightness
  //   this.exampleStates.Brightness = value as number;

  //   this.platform.log.debug('Set Characteristic Brightness -> ', value);
  // }

  // async setColor(value: string) {
  //   const success = await this.apiService.fill(value);
  //   if (!success) {
  //     return;
  //   }

  //   this.state.Colors = `[${value}]`;
  //   this.platform.log.debug('Set Characteristic Color ->', value);
  // }

  async setColor(value: string) {
    const success = await this.apiService.fill(value);
    if (!success) {
      return;
    }

    this.state.Colors = `${value}`;
    this.platform.log.debug('Set Characteristic Color ->', value);
  }

  async getColor(): Promise<string> {
    const result = await this.apiService.getDevice();
    if (result === null) {
      throw new this.platform.api.hap.HapStatusError(this.platform.api.hap.HAPStatus.SERVICE_COMMUNICATION_FAILURE);
    }

    const color = result.colors;
    this.platform.log.debug('Get Characteristic Color ->', color);
    return color;
  }
}
